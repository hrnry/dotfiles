# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi


# Put your fun stuff here.


__STATUS__=0
function GET_EXIT_STATUS(){
  local STATUS=${PIPESTATUS[@]}
  local MAX=0
  for S in ${STATUS}; do
    if (( ${MAX} < ${S} )); then
      MAX=${S}
    fi
  done
  __STATUS__=${MAX}
}
PROMPT_COMMAND='GET_EXIT_STATUS;'${PROMPT_COMMAND//GET_EXIT_STATUS;/}
function EXIT_STATUS_COLOR(){  [[ ${__STATUS__} == 0 ]] && printf '\e[01;32m' || printf '\e[01;31m' ;  }
function EXIT_STATUS_SIGN() {  [[ ${__STATUS__} == 0 ]] && printf '\u2713'    || printf '\u2717'    ;  }  # 2713:✓ 2717:✗
## 非表示文字はシーケンスの開始 '\[' と終了 '\]' で囲わなければ折り返し等が発生するとずれる!
PS1="\[\e]0;\u@\h:\w\007\]\[\e[01;32m\]\u@\h\[\e[01;34m\] \w\n\[\$(EXIT_STATUS_COLOR)\]\$(EXIT_STATUS_SIGN) \[\e[01;36m\]\t\[\e[01;34m\] \$\[\e[00m\] "

alias ffmpeg='ffmpeg -hide_banner'
alias ffprobe='ffprobe -hide_banner'

# ignoreboth(ignoredups + ignorespace), erasedups
export HISTCONTROL="$HISTCONTROL:ignoreboth"
export HISTIGNORE="$HISTIGNORE:exit"

export PYTHONWARNINGS=default

export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Djdk.gtk.version=3 -Dsun.java2d.opengl=true'
