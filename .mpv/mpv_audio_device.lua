--[[
Auto select 'USB_AUDIO_DAC' audio-device 

Add a line to ~/.mpv/config
  script='~/.mpv/mpv_audio_device.lua'
]]

require 'mp'

function auto_select()
  print('SCRIPT: ~/.mpv/' .. mp.get_script_name() ..'.lua')
  local adl = mp.get_property_native('audio-device-list')
  for n = 1, #adl do
    mp.msg.verbose('audio-device-list#' .. n .. '=' .. adl[n].name)
    if string.match(adl[n].name, 'USB_AUDIO_DAC') ~= nil then
      mp.msg.verbose('USB_AUDIO_DAC found!')
      mp.set_property_native('audio-device', adl[n].name)
      break
    end
  end
  print('audio-device='.. mp.get_property_native('audio-device'))
end

-- mp.register_event('file-loaded', auto_select)
mp.register_event('start-file', auto_select)
